﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace core
{
    class ChatLog
    {
        public static void WriteLine(String text)
        {
            if (Settings.Get<bool>("logging"))
            {
                try
                {
                    DateTime d = DateTime.Now;

					String path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t", "ChatLogs");

                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    path = Path.Combine(path, string.Format("ChatLog-{0:yyyy-MM-dd}.log", DateTime.Now));

                    using (StreamWriter writer = File.Exists(path) ? File.AppendText(path) : File.CreateText(path))
                        writer.WriteLine(d.ToShortTimeString() + " " + text);
                }
                catch { }
            }
        }
    }
}
