﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Reflection;
using System.Xml.Linq;

namespace core.Udp
{
    class UdpNodeManager
    {
        private static List<UdpNode> Nodes { get; set; }
        private static String DataPath { get; set; }

        public static UdpNode Find(Predicate<UdpNode> condition)
        {
            return Nodes.Find(condition);
        }

        public static void Initialize()
        {

            DataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");

            if (!Directory.Exists(DataPath))
                Directory.CreateDirectory(DataPath);

            DataPath = Path.Combine(DataPath, "nodes.xml");

            if (!LoadList())
                LoadDefaultList();
        }

        public static void Add(EndPoint ep)
        {
            UdpNode node = new UdpNode();
            node.IP = ((IPEndPoint)ep).Address;
            node.Port = (ushort)((IPEndPoint)ep).Port;
            Add(node);
        }

        public static void Add(UdpNode server)
        {
            if (!server.IP.Equals(Settings.ExternalIP))
            {
                UdpNode sobj = Nodes.Find(s => s.IP.Equals(server.IP));

                if (sobj != null)
                    sobj.Port = server.Port;
                else
                {
                    if (Nodes.Count > 4000)
                    {
                        Nodes.Sort((x, y) => x.LastConnect.CompareTo(y.LastConnect));
                        Nodes.RemoveRange(0, 250);
                    }

                    Nodes.Add(server);
                }
            }
        }

        public static void Expire(ulong time)
        {
            Nodes.RemoveAll(s => s.Try > 4 &&
                (time - s.LastSentIPS) > 60000 &&
                (time - s.LastConnect) > 3600000);

            if (Nodes.Count < 10)
            {
                ServerCore.Log("local node list expired");
                LoadDefaultList();
            }
        }

        public static void Update(ulong time)
        {
            Nodes.ForEachWhere(x => x.Ack = 40000, x => x.Ack > 65000);

            try
            {
                var linq = from x in Nodes
                           where x.Ack > 0 && (time - x.LastConnect) < 1800000
                           orderby x.Ack descending
                           select x;

                int counter = 0;
                XDocument xml = new XDocument(new XElement("NodeList"));

                foreach (UdpNode i in linq)
                {
                    xml.Element("NodeList").Add(new XElement("node",
                        new XElement("ip", i.IP),
                        new XElement("port", i.Port),
                        new XElement("ack", i.Ack > 65000 ? 40000 : i.Ack)));

                    if (++counter == 100)
                        break;
                }

                xml.Save(DataPath);
            }
            catch { }

            if (UdpStats.ACKIPS == 0)
                LoadDefaultList();
        }

        private static bool LoadList()
        {
            try
            {
                XDocument xml = XDocument.Load(DataPath);

                Nodes = (from i in xml.Descendants("node")
                         select new UdpNode
                         {
                             IP = IPAddress.Parse(i.Element("ip").Value),
                             Port = ushort.Parse(i.Element("port").Value),
                             Ack = int.Parse(i.Element("ack").Value)
                         }).ToList<UdpNode>();


                if (Nodes.Count == 0)
                    return false;

                ServerCore.Log("local node list loaded [" + Nodes.Count + "]");
                return true;
            }
            catch { }

            return false;
        }

        private static void LoadDefaultList()
        {
            Nodes = new List<UdpNode>();

	        string dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
	        Stream stream = null;

	        if (string.IsNullOrWhiteSpace(dir) || !File.Exists(Path.Combine(dir, "servers.dat")))
	        {
				ServerCore.Log("default node list not found");

				ServerCore.Log("downloading node list from internet");
		        using (WebClient wc = new WebClient())
		        {
			        byte[] dataBytes = wc.DownloadData("http://chatrooms.marsproject.net/list/cache.aspx");
			        stream = new MemoryStream(dataBytes);
		        }
	        }
	        else
	        {
		        stream = new FileStream(Path.Combine(dir, "servers.dat"), FileMode.Open, FileAccess.Read);
	        }
            using (stream)
            {
                byte[] buffer = new byte[6];

                while (stream.Read(buffer, 0, buffer.Length) == 6)
                {
                    UdpNode node = new UdpNode();
                    node.IP = new IPAddress(buffer.Take(4).ToArray());
                    node.Port = BitConverter.ToUInt16(buffer.ToArray(), 4);
                    Nodes.Add(node);
                }
            }

            ServerCore.Log("default node list loaded");
        }

        public static UdpNode[] GetServers(int max_servers, ulong time)
        {
            Nodes.Randomize();

            List<UdpNode> results = Nodes.FindAll(s => s.Ack > 0
                && (s.LastConnect + 900000) > time);

            if (results.Count > max_servers)
                results = results.GetRange(0, max_servers);

            return results.ToArray();
        }

        public static UdpNode[] GetServers(IPAddress target_ip, int max_servers, ulong time)
        {
            Nodes.Randomize();

            List<UdpNode> results = Nodes.FindAll(s => !s.IP.Equals(target_ip)
                && s.Ack > 0 && (s.LastConnect + 900000) > time);

            if (results.Count > max_servers)
                results = results.GetRange(0, max_servers);

            return results.ToArray();
        }

        public static UdpNode[] GetServers()
        {
            return Nodes.ToArray();
        }

        public static UdpNode NextPusher(ulong time)
        {
            if (Nodes.Count == 0)
                return null;

            Nodes.Sort((x, y) => x.LastSentIPS.CompareTo(y.LastSentIPS));

            if ((Nodes[0].LastSentIPS + 900000) < time)
            {
                Nodes[0].LastSentIPS = time;
                Nodes[0].Try++;
                return Nodes[0];
            }

            return null;
        }
    }
}
