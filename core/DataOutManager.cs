﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace core
{
	class DataOutManager
	{
		private List<byte> list = new List<byte>();
		private object padlock = new object();

		public bool HasData
		{
			get
			{
				bool result = true;

				lock (this.padlock)
					result = this.list.Count > 0;

				return result;
			}
		}

		public void Enqueue(byte[] data)
		{
			lock (this.padlock)
				list.AddRange(data);
		}

		public void Insert(byte[] data)
		{
			lock (this.padlock)
				list.InsertRange(0, data);
		}

		public byte[] Dequeue()
		{
			byte[] result = null;

			lock (this.padlock)
			{
				if (this.list.Count < 1024)
				{
					result = this.list.ToArray();
					this.list.Clear();
				}
				else
				{
					result = this.list.Take(1024).ToArray();
					this.list.RemoveRange(0, 1024);
				}
			}

			return result;
		}

		public void Clear()
		{
			lock (this.padlock)
			{
				this.list.Clear();
			}
		}
	}
}