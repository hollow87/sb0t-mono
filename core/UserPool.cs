﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace core
{
    class UserPool
    {
        public static List<AresClient> AUsers;
        public static List<ib0t.ib0tClient> WUsers;
        public static List<WebWorker> WW;

        public static void Build()
        {
            AUsers = new List<AresClient>();
            WUsers = new List<ib0t.ib0tClient>();
            WW = new List<WebWorker>();
        }

        public static void Destroy()
        {
            try
            {
                AUsers.ForEach(x => x.LoggedIn = false);
                AUsers.ForEach(x => x.Disconnect());
                AUsers.Clear();
                AUsers = null;
            }
            catch { }

            try
            {
                WUsers.ForEach(x => x.LoggedIn = false);
                WUsers.ForEach(x => x.Disconnect());
                WUsers.Clear();
                WUsers = null;
            }
            catch { }

            try
            {
                WW.ForEach(x => x.Disconnect());
                WW.Clear();
                WW = null;
            }
            catch { }
        }

        public static void CreateAresClient(Socket sock, ulong time)
        {
            for (ushort u = 0; u < ushort.MaxValue; u++)
            {
                int index = AUsers.FindIndex(x => x.ID == u);

                if (index == -1)
                {

	                try
	                {
		                AUsers.Add(new AresClient(sock, time, u));
	                }
	                catch (Exception e)
	                {
		                ServerCore.Log(e.Message, e);
	                }
                    AUsers.Sort((x, y) => x.ID.CompareTo(y.ID));
                    break;
                }
            }
        }

        public static void CreateIb0tClient(AresClient client, ulong time)
        {
            for (ushort u = 700; u < ushort.MaxValue; u++)
            {
                int index = WUsers.FindIndex(x => x.ID == u);

                if (index == -1)
                {
                    WUsers.Add(new ib0t.ib0tClient(client, time, u));
                    WUsers.Sort((x, y) => x.ID.CompareTo(y.ID));
                    client.Sock = null;
                    AUsers.RemoveAll(x => x.ID == client.ID);
                    break;
                }
            }
        }

        public static void CreateWW(AresClient client)
        {
	        try
	        {
		        if (client.Connected)
		        {
			        WW.Add(new WebWorker(client));
			        AUsers.RemoveAll(x => x.ID == client.ID);
		        }
	        }
	        catch (SocketException e)
	        {
		        if (e.SocketErrorCode != SocketError.NotConnected)
		        {
			        ServerCore.Log(e.Message, e);
		        }
	        }
        }

        public static ushort UserCount
        {
            get
            {
                ushort result = (ushort)AUsers.FindAll(x => x.LoggedIn).Count;
                result += (ushort)WUsers.FindAll(x => x.LoggedIn).Count;
                return result;
            }
        }
    }
}
