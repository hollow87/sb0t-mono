﻿using iconnect;

namespace scripting
{
    public partial class ServerEvents
    {
        public ServerEvents(IHostApp cb)
        {
            Server.SetCallback(cb);
        }

        public void Dispose() { }
        public void Load() { }
        public void UnhandledProtocol(IUser client, bool custom, byte msg, byte[] packet) { }
    }
}
