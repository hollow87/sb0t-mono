﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jurassic;
using Jurassic.Library;
using System.Reflection;
using System.Linq.Expressions;
namespace scripting
{
	public static class JurassicExtenstions
	{
		 public void EmbedGlobalClass(Type global)
        {
            Type funcType;
            IEnumerable<MethodInfo> methodInfos = ((IEnumerable<MethodInfo>)global.GetMethods()).Where<MethodInfo>((MethodInfo x) => {
                if (!x.IsPublic)
                {
                    return false;
                }
                return x.IsStatic;
            });
            foreach (MethodInfo methodInfo in methodInfos)
            {
                List<Type> list = (
                    from x in (IEnumerable<ParameterInfo>)methodInfo.GetParameters()
                    select x.ParameterType).ToList<Type>();
                if (methodInfo.ReturnType != typeof(void))
                {
                    list.Add(methodInfo.ReturnType);
                    funcType = Expression.GetFuncType(list.ToArray());
                }
                else
                {
                    funcType = Expression.GetActionType(list.ToArray());
                }
                Delegate @delegate = Delegate.CreateDelegate(funcType, null, methodInfo);
                string name = methodInfo.Name;
                JSFunctionAttribute customAttribute = (JSFunctionAttribute)Attribute.GetCustomAttribute(methodInfo, typeof(JSFunctionAttribute), false);
                JSFunctionFlags flags = JSFunctionFlags.None;
                if (customAttribute != null)
                {
                    if (!string.IsNullOrEmpty(customAttribute.Name))
                    {
                        name = customAttribute.Name;
                    }
                    flags = customAttribute.Flags;
                }
                this.SetGlobalValue(name, new ClrFunction(this.Function.InstancePrototype, @delegate, true, name, flags));
                bool flag = true;
                this.Global.DefineProperty(name, new PropertyDescriptor(this.Global[name], PropertyAttributes.Enumerable), flag);
            }
        }

        public void EmbedInstance(string name, Type type)
        {
            if (!type.IsSubclassOf(typeof(ClrFunction)))
            {
                throw new ArgumentException(string.Concat(type.FullName, " does not inherit from ClrFunction"));
            }
            object[] objArray = new object[] { this };
            this.SetGlobalValue(name, Activator.CreateInstance(type, objArray));
        }

        public void EmbedInstances(Type[] types)
        {
            Type[] typeArray = types;
            for (int i = 0; i < (int)typeArray.Length; i++)
            {
                Type type = typeArray[i];
                JSEmbed customAttribute = (JSEmbed)Attribute.GetCustomAttribute(type, typeof(JSEmbed), false);
                if (customAttribute != null)
                {
                    string name = customAttribute.Name;
                    if (string.IsNullOrEmpty(name))
                    {
                        name = type.Name;
                    }
                    this.EmbedInstance(name, type);
                }
            }
        }

        public void EmbedObjectPrototype(string name, Type type)
        {
            if (!type.IsSubclassOf(typeof(ClrFunction)))
            {
                throw new ArgumentException(string.Concat(type.FullName, " does not inherit from ClrFunction"));
            }
            object[] objArray = new object[] { this };
            object obj = Activator.CreateInstance(type, objArray);
            bool flag = true;
            this.Global.DefineProperty(name, new PropertyDescriptor(obj, PropertyAttributes.Enumerable), flag);
        }

        public void EmbedObjectPrototypes(Type[] types)
        {
            Type[] typeArray = types;
            for (int i = 0; i < (int)typeArray.Length; i++)
            {
                Type type = typeArray[i];
                JSEmbed customAttribute = (JSEmbed)Attribute.GetCustomAttribute(type, typeof(JSEmbed), false);
                if (customAttribute != null)
                {
                    string name = customAttribute.Name;
                    if (string.IsNullOrEmpty(name))
                    {
                        name = type.Name;
                    }
                    this.EmbedObjectPrototype(name, type);
                }
            }
        }

        public void EmbedStatic(string name, Type type)
        {
            if (!type.IsSubclassOf(typeof(ObjectInstance)))
            {
                throw new ArgumentException(string.Concat(type.FullName, " does not inherit from ObjectInstance"));
            }
            object[] objArray = new object[] { this };
            object obj = Activator.CreateInstance(type, objArray);
            bool flag = true;
            this.Global.DefineProperty(name, new PropertyDescriptor(obj, PropertyAttributes.Enumerable), flag);
        }

        public void EmbedStatics(Type[] types)
	}
}
