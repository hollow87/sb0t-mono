﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iconnect;

namespace commands
{
    class LogSend
    {
        private static List<ushort> list { get; set; }

        public static void Reset()
        {
            list = new List<ushort>();
        }

        public static void Log(IUser client, String text)
        {
            ILevel l = Server.GetLevel("logsend");
            String bot = Server.Chatroom.BotName;

            Server.Users.All(x =>
            {
                if (!x.Link.IsLinked)
                    if (x.Level >= l)
                        if (list.Contains(x.ID))
                            x.PM(bot, "LOGSEND: " + client.Name + "> /" + text);
            });

	        try
	        {
		        DateTime d = DateTime.Now;
		        String path = Path.Combine(Server.DataPath, string.Format("adminlog-{0:yyyy-MM-dd}.log", DateTime.Now));

		        StringBuilder sb = new StringBuilder();
		        sb.AppendFormat("{0} {1} {2}> /{3}", d.ToShortDateString(), d.ToShortTimeString(), client.Name, text);
		        sb.AppendLine();

		        File.AppendAllText(path, sb.ToString());
	        }
	        catch (Exception e)
	        {
		        Server.WriteLog(e.Message + Environment.NewLine + e.StackTrace);
	        }
        }

        public static void Add(IUser client)
        {
            if (list.FindIndex(x => x == client.ID) == -1)
                list.Add(client.ID);
        }

        public static void Remove(IUser client)
        {
            list.RemoveAll(x => x == client.ID);
        }

        public static bool Has(IUser client)
        {
            return list.FindIndex(x => x == client.ID) > -1;
        }
    }
}
