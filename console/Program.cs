﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using core;
using System.Threading;
using System.Net;
using System.Web;
using System.Net.Sockets;
using System.IO;
using Mono.Unix;
using Mono.Unix.Native;

namespace console
{
	public class Program
	{
		private static ServerCore _server = null;
		private static Thread _thread = null;
		private static bool _running = true;
		private static void Main(string[] args)
		{
			UnixSignal intr = new UnixSignal(Signum.SIGINT);
			UnixSignal term = new UnixSignal(Signum.SIGTERM);
			UnixSignal hup = new UnixSignal(Signum.SIGHUP);
			UnixSignal usr2 = new UnixSignal(Signum.SIGUSR2);

			UnixSignal[] signals = new UnixSignal[] { term, hup, usr2 };

			_thread = new Thread(RunThread);
			_thread.Start();

			_running = true;

			while (_running)
			{
				int idx = UnixSignal.WaitAny(signals);

				if (idx < 0 || idx >= signals.Length) continue;

				if (term.IsSet)
				{
					term.Reset();

					_running = false;
				}
				else if (hup.IsSet)
				{
					hup.Reset();
				}
				else if(intr.IsSet)
				{
					intr.Reset();
				}
				else if (usr2.IsSet)
				{
					usr2.Reset();
				}
			}

			
		}

		static void RunThread()
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
			
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");
			string pidPath = Path.Combine(dataDir, "sb0t.pid");
			
			_server = new ServerCore();
			ServerCore.LogUpdate += ServerCore_LogUpdate;

			core.Extensions.ExtensionManager.Setup();
			_server.Open();

			var proc = Process.GetCurrentProcess();
			File.WriteAllText(pidPath, proc.Id.ToString(CultureInfo.InvariantCulture));

			string avatarDir = Path.Combine(dataDir, "Avatars");

			if (!Directory.Exists(avatarDir))
				Directory.CreateDirectory(avatarDir);

			string defaultAvatar = Path.Combine(avatarDir, "default");
			string serverAvatar = Path.Combine(avatarDir, "server");

			if (File.Exists(defaultAvatar))
				Avatars.UpdateDefaultAvatar(File.ReadAllBytes(defaultAvatar));

			if (File.Exists(serverAvatar))
				Avatars.UpdateServerAvatar(File.ReadAllBytes(serverAvatar));

			while(_server.Running && _running)
			{
				Thread.Sleep(100);
			}

			Console.WriteLine("Exited");
		}

		private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine(DateTime.Now.ToString("yy/MM/dd HH:mm:ss zzz"));
			Exception e = unhandledExceptionEventArgs.ExceptionObject as Exception;

			if (e == null)
				return;

			sb.AppendLine(e.Message);
			sb.AppendLine(e.StackTrace);
			sb.AppendLine(e.Source);

			if (e.InnerException != null)
			{
				sb.AppendLine("Inner Exception------");
				sb.AppendLine(e.InnerException.Message);
				sb.AppendLine(e.InnerException.StackTrace);
				sb.AppendLine(e.Source);
			}

			File.AppendAllText(GetLogPath(), sb.ToString());

		}

		static string GetLogPath()
		{
			return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t",
				string.Format("ServerLog-{0:yyyy-MM-dd}.log", DateTime.Now));
		}
		static void ServerCore_LogUpdate(object sender, ServerLogEventArgs e)
		{
			StringBuilder sb = new StringBuilder();

			if (e != null)
			{
				sb.AppendLine(DateTime.Now.ToString("yy/MM/dd HH:mm:ss zzz"));
				sb.AppendLine(e.Message);

				if (e.Error != null)
				{
					sb.AppendLine(e.Error.ToString());
					sb.AppendLine(e.Error.StackTrace);
				}

				sb.AppendLine("----------------------------------------");

				File.AppendAllText(GetLogPath(), sb.ToString());
			}
			
		}
	}
}
