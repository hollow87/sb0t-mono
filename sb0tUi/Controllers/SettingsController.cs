﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace sb0tUi.Controllers
{
	
	[Authorize]
    public class SettingsController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
			return View();
        }

		[HttpPost]
		public ActionResult Upload()
		{

			try
			{
				if (!string.IsNullOrWhiteSpace(Request.Headers["X_FILENAME"]))
				{
					if (Request.InputStream.Length < 3*1024*1024)
					{
						if(string.IsNullOrWhiteSpace(Request.Headers["X_TYPE"]))
							return Content("failure");

						var type = Request.Headers["X_TYPE"];

						using (var memoryStream = new MemoryStream())
						{
							Request.InputStream.CopyTo(memoryStream);
							byte[] data = Resize(memoryStream.ToArray());

							var dataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t", "Avatars");
							if (!Directory.Exists(dataPath))
								Directory.CreateDirectory(dataPath);

							dataPath = Path.Combine(dataPath, type);

							System.IO.File.WriteAllBytes(dataPath, data);
						}
						return Content(Request.Headers["X_FILENAME"] + " uploaded");
					}
				}
			}
			catch (Exception e)
			{
				return Content("failure");
			}


			return Content("failure");

		}

		private static byte[] Resize(byte[] data)
		{
			byte[] result = null;

			using (MemoryStream org_ms = new MemoryStream(data))
			using (Bitmap org = new Bitmap(org_ms))
			using (Bitmap bmp = new Bitmap(48, 48))
			using (Graphics g = Graphics.FromImage(bmp))
			{
				g.Clear(Color.White);
				g.CompositingQuality = CompositingQuality.HighQuality;
				g.InterpolationMode = InterpolationMode.HighQualityBilinear;
				g.SmoothingMode = SmoothingMode.HighQuality;
				g.DrawImage(org, new RectangleF(0, 0, 48, 48));

				using (MemoryStream resized_ms = new MemoryStream())
				{
					ImageCodecInfo info = ImageCodecInfo.GetImageEncoders().FirstOrDefault(x => x.MimeType == "image/jpeg");

					using (EncoderParameters encoding = new EncoderParameters())
					{
						encoding.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 75L);
						bmp.Save(resized_ms, info, encoding);
						result = resized_ms.ToArray();
						encoding.Param[0].Dispose();
					}
				}
			}

			return result;
		}
    }
}
