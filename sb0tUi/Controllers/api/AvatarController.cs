﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sb0tUi.Controllers.api
{
	[Authorize]
    public class AvatarController : ApiController
    {
		public byte[] Get(string type)
		{
			var dataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t", "Avatars");
			if (!Directory.Exists(dataPath))
				return null;

			dataPath = Path.Combine(dataPath, type);

			if (!File.Exists(dataPath))
				return null;

			return File.ReadAllBytes(dataPath);

			
		}
    }
}
