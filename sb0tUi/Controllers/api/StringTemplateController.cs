﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sb0tUi.Controllers.api
{
    public class StringTemplateController : ApiController
    {
		public string Get()
		{
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");

			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);

			string filePath = Path.Combine(dataDir, "strings.txt");

			if (!File.Exists(filePath))
				return null;

			return File.ReadAllText(filePath);
		}

		public void Post([FromBody] string motd)
		{
			if (motd == null)
				motd = string.Empty;

			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");
			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);

			string filePath = Path.Combine(dataDir, "strings.txt");

			File.WriteAllText(filePath, motd);
		}
    }
}
