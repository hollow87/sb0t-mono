﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
namespace sb0tUi.Controllers.api
{
	[Authorize]
    public class AdminLogController : ApiController
    {
		public DateTime[] Get()
		{
			//ServerLog-2014-06-30.log
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");
			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);

			var files = Directory.EnumerateFiles(dataDir, "adminlog-*.log");

			List<DateTime> dates = new List<DateTime>();
			foreach (var file in files)
			{
				var fileNoExt = file.Replace(".log", "");

				var fileParts = fileNoExt.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
				if (fileParts.Length < 4)
					return null;

				int year = int.Parse(fileParts[1]);
				int month = int.Parse(fileParts[2]);
				int day = int.Parse(fileParts[3]);

				DateTime date = new DateTime(year, month, day);
				dates.Add(date);
			}

			return dates.ToArray();
		}

		public string[] Get(DateTime date)
		{
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t");
			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);


			string fileName = string.Format("adminlog-{0:D2}-{1:D2}-{2:D2}.log", date.Year, date.Month, date.Day);

			string filePath = Path.Combine(dataDir, fileName);
			if(File.Exists(filePath))
				return File.ReadAllLines(filePath);

			return null;
		}
    }
}
