﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Web.Http;
using core;
using sb0tUi.Models;

namespace sb0tUi.Controllers.api
{
	[Authorize]
	public class SbotSettingsController : ApiController
	{
		private readonly static Random RandomGen = new Random();
		private static string RandomPass()
		{
			var count = RandomGen.Next(8, 11);
			var sb = new StringBuilder(count, count);

			for (var i = 0; i < count; i++)
			{
				sb.Append((char)('a' + RandomGen.Next(0, 26)));
			}

			return sb.ToString();
		}

		// GET api/<controller>?section=<section>&type=<type>
		public IEnumerable<SettingsModel> Get(int section, string type)
		{
			switch (section)
			{
				case 0:
				{
					#region Main settings

					switch (type)
					{
							#region String

						case "string":
							return new List<SettingsModel>
							{
								Get("RoomName"),
								Get("BotName"),
								Get("Port")
							};

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("Logging"),
								Get("Scribbles"),
								Get("ChanList"),
								Get("VoiceChat"),
								Get("ib0tSupport")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>();

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 1:
				{
					#region Admin Settings

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>
							{
								Get("OwnerPassword")
							};

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("Commands"),
								Get("Unreg"),
								Get("StrictMode")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>
							{
								#region Admin Commmands

								Get("addgreetmsg"),
								Get("addtopic"),
								Get("addurl"),
								Get("adminannounce"),
								Get("adminmsg"),
								Get("admins"),
								Get("announce"),
								Get("anon"),
								Get("ban"),
								Get("ban10"),
								Get("ban60"),
								Get("bansend"),
								Get("banstats"),
								Get("caps"),
								Get("cbans"),
								Get("changemessage"),
								Get("changename"),
								Get("clearscreen"),
								Get("cloak"),
								Get("clock"),
								Get("clone"),
								Get("colors"),
								Get("customname"),
								Get("customnames"),
								Get("define"),
								Get("disableadmins"),
								Get("disableavatar"),
								Get("echo"),
								Get("filter"),
								Get("general"),
								Get("greetmsg"),
								Get("history"),
								Get("idle"),
								Get("ipsend"),
								Get("kewltext"),
								Get("kick"),
								Get("kiddy"),
								Get("lastseen"),
								Get("link"),
								Get("listbans"),
								Get("listgreetmsg"),
								Get("listquarantined"),
								Get("listrangebans"),
								Get("listurls"),
								Get("loadmotd"),
								Get("loadtemplate"),
								Get("logsend"),
								Get("lower"),
								Get("move"),
								Get("mtimeout"),
								Get("muzzle"),
								Get("oldname"),
								Get("paint"),
								Get("pmgreetmsg"),
								Get("pmroom"),
								Get("rangeban"),
								Get("rangeunban"),
								Get("redirect"),
								Get("remgreetmsg"),
								Get("remtopic"),
								Get("remurl"),
								Get("roominfo"),
								Get("roomsearch"),
								Get("sharefiles"),
								Get("stats"),
								Get("status"),
								Get("stealth"),
								Get("trace"),
								Get("unban"),
								Get("uncustomname"),
								Get("unlink"),
								Get("unlower"),
								Get("unquarantine"),
								Get("urban"),
								Get("url"),
								Get("vspy"),
								Get("whois"),
								Get("whowas")
								#endregion
							};

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 2:
				{
					#region Advanced Settings

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>
							{
								Get("ib0tUrl"),
								Get("UDPEndPoint")
							};

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("EnableFileBrowse"),
								Get("MaskIPs"),
								Get("EnableRoomSearch"),
								Get("LocalLogin")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>
							{
								Get("Language")
							};

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 3:
				{
					#region Scripting

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>();

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("JSEnable"),
								Get("InRoomScripting"),
								Get("ScriptLevelChange")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>
							{
								Get("Scripting Level")
							};

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 4:
				{
					#region Captcha

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>();

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("CaptchaEnabled")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>
							{
								Get("CaptchaMode")
							};

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 5:
				{
					#region Restrictions

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>
							{
								Get("MinAge")
							};

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("AgeRestrictEnabled"),
								Get("RejectMale"),
								Get("RejectFemale"),
								Get("RejectUnknown"),
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>();

							#endregion

						default:
							return null;
					}

					#endregion
				}

				case 6:
				{
					#region Ban List

					switch (type)
					{
							#region string

						case "string":
							return new List<SettingsModel>
							{
								Get("BanClearInterval")
							};

							#endregion

							#region boolean

						case "boolean":
							return new List<SettingsModel>
							{
								Get("BanClearEnabled")
							};

							#endregion

							#region object

						case "object":
							return new List<SettingsModel>();

							#endregion

						default:
							return null;
					}

					#endregion
				}
				default:
					return null;
			}
		}

		// GET api/<controller>?name=<name>
		public SettingsModel Get(string name)
		{
			switch (name)
			{
					#region Main Settings

				case "RoomName":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Room Name",
						Value = ((Func<string>)(() =>
						{
							if (string.IsNullOrWhiteSpace(Settings.Get<string>("name")))
								Settings.Set("name", "my chatroom");

							return Settings.Get<string>("name");
						}))()
						
					};
				case "BotName":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Bot Name",
						Value = ((Func<string>)(() =>
						{
							if (string.IsNullOrWhiteSpace(Settings.Get<string>("bot")))
								Settings.Set("bot", "sb0t");
							

							return Settings.Get<string>("bot");
						}))()
					};
				case "Port":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Port",
						Value = ((Func<string>)(() =>
						{
							if (Settings.Get<int>("port") == 0)
								Settings.Set("port", 54321);

							return Settings.Get<int>("port").ToString(CultureInfo.InvariantCulture);
						}))()
					};
				case "Logging":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Chat logging",
						Value = Settings.Get<bool>("logging")
					};
				case "Scribbles":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Scribbles",
						Value = Settings.Get<bool>("can_room_scribble")
					};
				case "ChanList":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "List room",
						Value = Settings.Get<bool>("udp")
					};
				case "VoiceChat":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "Voice Chat",
						Value = Settings.Get<bool>("voice")
					};
				case "ib0tSupport":
					return new SettingsModel
					{
						Name = name,
						Section = "Main",
						Label = "ib0t Support",
						Value = Settings.Get<bool>("enabled", "web")
					};

					#endregion

					#region Admin Settings

				case "OwnerPassword":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = "Owner password",
						Value = ((Func<string>) (() =>
						{
							if (string.IsNullOrWhiteSpace(Settings.Get<string>("owner")))
								Settings.Set("owner", RandomPass());

							return Settings.Get<string>("owner");
						}))()
					};
				case "Commands":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = "Built-in Commands",
						Value = Settings.Get<bool>("commands")
					};
				case "Unreg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = "Unregistered users can use commands",
						Value = Settings.Get<bool>("allow_unreg")
					};
				case "StrictMode":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = "Check paswords against clients (strict mode)",
						Value = Settings.Get<bool>("strict")
					};

					#endregion

					#region Admin Commands

				case "addgreetmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "addtopic":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "addurl":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "adminannounce":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "adminmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "admins":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "announce":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "anon":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "ban":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "ban10":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "ban60":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "bansend":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "banstats":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "caps":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "cbans":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "changemessage":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "changename":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "clearscreen":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "cloak":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "clock":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "clone":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "colors":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "customname":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "customnames":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "define":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "disableadmins":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "disableavatar":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "echo":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "filter":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "general":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "greetmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "history":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "idle":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "ipsend":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "kewltext":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "kick":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "kiddy":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "lastseen":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "link":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "listbans":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "listgreetmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "listquarantined":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "listrangebans":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "listurls":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "loadmotd":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "loadtemplate":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "logsend":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "lower":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "move":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "mtimeout":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "muzzle":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "oldname":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "paint":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "pmgreetmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "pmroom":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "rangeban":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "rangeunban":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "redirect":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "remgreetmsg":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "remtopic":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "remurl":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "roominfo":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "roomsearch":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "sharefiles":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "stats":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "status":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "stealth":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "trace":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "unban":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "uncustomname":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "unlink":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "unlower":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "unquarantine":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "urban":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "url":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 3, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "vspy":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 2, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "whois":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};
				case "whowas":
					return new SettingsModel
					{
						Name = name,
						Section = "Admin",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{254, "Disabled"}
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte(name, "commands");
							if (data == null)
								Settings.Set(name, 1, "commands");

							return Settings.Get<byte>(name, "commands");
						}))()
					};

					#endregion

					#region Advanced

				case "EnableFileBrowse":
					return new SettingsModel
					{
						Name = name,
						Section = "Advanced",
						Label = "Enable file browsing",
						Value = Settings.Get<bool>("files")
					};
				case "MaskIPs":
					return new SettingsModel
					{
						Name = name,
						Section = "Advanced",
						Label = "Hide IP addresses",
						Value = Settings.Get<bool>("hide_ips")
					};
				case "EnableRoomSearch":
					return new SettingsModel
					{
						Name = "EnableRoomSearch",
						Section = "Advanced",
						Label = "Enable room search",
						Value = Settings.Get<bool>("roomsearch")
					};
				case "LocalLogin":
					return new SettingsModel
					{
						Name = name,
						Section = "Advanced",
						Label = "Local client auto login",
						Value = Settings.Get<bool>("local_host")
					};
				case "ib0tUrl":
					return new SettingsModel
					{
						Name = "ib0tUrl",
						Section = "Advanced",
						Label = "ib0t channel list receiver url",
						Value = ((Func<string>)(() =>
						{
							if (string.IsNullOrWhiteSpace(Settings.Get<string>("url", "web")))
								Settings.Set("url", "http://chatrooms.marsproject.net/ibot.aspx", "web");

							return Settings.Get<string>("url", "web");
						}))()
					};
				case "UDPEndPoint":
					return new SettingsModel
					{
						Name = name,
						Section = "Advanced",
						Label = "UDP local endpoint",
						Value = ((Func<string>)(() =>
						{
							if (Settings.Get<byte[]>("udp_address") == null)
								Settings.Set("udp_address", new byte[] { 0, 0, 0, 0});

							return new IPAddress(Settings.Get<byte[]>("udp_address")).ToString();
						}))()
					};
				case "Language":
					return new SettingsModel
					{
						Name = name,
						Section = "Advanced",
						Label = new Dictionary<byte, string>
						{
							{11, "Arabic"},
							{12, "Chinese"},
							{14, "Czech"},
							{15, "Danish"},
							{16, "Dutch"},
							{10, "English"},
							{27, "Finnish"},
							{28, "French"},
							{29, "German"},
							{30, "Italian"},
							{17, "Japanese"},
							{19, "Kirghiz"},
							{20, "Polish"},
							{21, "Portuguese"},
							{31, "Russian"},
							{22, "Slovak"},
							{23, "Spanish"},
							{25, "Swedish"},
							{26, "Turkish"},
						},
						Value = ((Func<byte>)(() =>
						{
							if (Settings.Get<byte>("language") == 0)
								Settings.Set("language", 10);

							return Settings.Get<byte>("language");
						}))()
					};

					#endregion

					#region Scripting 

				case "JSEnable":
					return new SettingsModel
					{
						Name = name,
						Section = "Scripting",
						Label = "Enable javascript engine",
						Value = Settings.Get<bool>("scripting")
					};
				case "InRoomScripting":
					return new SettingsModel
					{
						Name = name,
						Section = "Scripting",
						Label = "Enable in-room scripting",
						Value = Settings.Get<bool>("inroom_scripting")
					};
				case "ScriptLevelChange":
					return new SettingsModel
					{
						Name = name,
						Section = "Scripting",
						Label = "Allow scripts to change user levels",
						Value = Settings.Get<bool>("script_can_level")
					};
				case "Scripting Level":
					return new SettingsModel
					{
						Name = name,
						Section = "Scripting",
						Label = new Dictionary<byte, string>
						{
							{1, "Moderator"},
							{2, "Administrator"},
							{3, "Host"},
							{4, "Room Owner"}
						},
						Value = ((Func<byte>)(() =>
						{
							if (Settings.Get<byte>("inroom_level") == 0)
								Settings.Set("inroom_level", 4);

							return Settings.Get<byte>("inroom_level");
						}))()
					};

					#endregion

					#region Captcha

				case "CaptchaEnabled":
					return new SettingsModel
					{
						Name = name,
						Section = "Captcha",
						Label = "Enable captcha",
						Value = Settings.Get<bool>("captcha")
					};
				case "CaptchaMode":
					return new SettingsModel
					{
						Name = name,
						Section = "Captcha",
						Label = new Dictionary<byte, string>
						{
							{0, "Regular"},
							{1, "Quarantine"},
						},
						Value = ((Func<byte>)(() =>
						{
							byte? data = Settings.GetNullableByte("captcha_mode");
							if (data == null)
								Settings.Set("captcha_mode", 0);

							return Settings.Get<byte>("captcha_mode");
						}))()
					};

					#endregion

					#region Restrictions

				case "AgeRestrictEnabled":
					return new SettingsModel
					{
						Name = name,
						Section = "Restrictions",
						Label = "Enable Age Restrictions",
						Value = Settings.Get<bool>("age_restrict")
					};
				case "RejectMale":
					return new SettingsModel
					{
						Name = name,
						Section = "Restrictions",
						Label = "Reject male",
						Value = Settings.Get<bool>("reject_male")
					};
				case "RejectFemale":
					return new SettingsModel
					{
						Name = name,
						Section = "Restrictions",
						Label = "Reject female",
						Value = Settings.Get<bool>("reject_female")
					};
				case "RejectUnknown":
					return new SettingsModel
					{
						Name = name,
						Section = "Restrictions",
						Label = "Reject unknown",
						Value = Settings.Get<bool>("reject_unknown")
					};
				case "MinAge":
					return new SettingsModel
					{
						Name = name,
						Section = "Restrictions",
						Label = "Minimum age",
						Value = ((Func<string>)(() =>
						{
							if (Settings.Get<int>("age_restrict_value") == 0)
								Settings.Set("age_restrict_value", 18);

							return Settings.Get<int>("age_restrict_value").ToString(CultureInfo.InvariantCulture);
						}))()
					};

					#endregion

					#region Ban Lists

				case "BanClearInterval":
					return new SettingsModel
					{
						Name = name,
						Section = "BanList",
						Label = "Clear Interval (hours)",
						Value = ((Func<string>)(() =>
						{
							if (Settings.Get<int>("auto_ban_clear_interval") == 0)
								Settings.Set("auto_ban_clear_interval", 1);

							return Settings.Get<int>("auto_ban_clear_interval").ToString(CultureInfo.InvariantCulture);
						}))()
					};
				case "BanClearEnabled":
					return new SettingsModel
					{
						Name = name,
						Section = "BanList",
						Label = "Auto clear bans",
						Value = Settings.Get<bool>("auto_ban_clear_enabled")
					};

					#endregion

				default:
					return null;
			}
		}

		public void Post(SettingsModel model)
		{
			switch (model.Name)
			{
					#region Main Settings

				case "RoomName":
				{
					Settings.Set("name", model.Value);
					break;
				}
				case "BotName":
				{
					Settings.Set("bot", model.Value);
					break;
				}
				case "Port":
				{
					var val = Convert.ToUInt16(model.Value);
					Settings.Set("port", val);
					break;
				}
				case "Logging":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("logging", val);
					break;
				}
				case "Scribbles":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("can_room_scribble", val);
					break;
				}
				case "ChanList":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("udp", val);
					break;
				}
				case "VoiceChat":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("voice", val);
					break;
				}
				case "ib0tSupport":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("enabled", val, "web");
					break;
				}

					#endregion

					#region Admin Settings

				case "OwnerPassword":
				{
					Settings.Set("owner", model.Value);
					break;
				}
				case "Commands":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("commands", val);
					break;
				}
				case "Unreg":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("allow_unreg", val);
					break;
				}
				case "StrictMode":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("strict", val);
					break;
				}

					#endregion

					#region Admin Commands

				case "addgreetmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "addtopic":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "addurl":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "adminannounce":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "adminmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "admins":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "announce":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "anon":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "ban":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "ban10":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "ban60":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "bansend":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "banstats":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "caps":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "cbans":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "changemessage":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "changename":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "clearscreen":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "cloak":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "clock":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "clone":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "colors":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "customname":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "customnames":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "define":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "disableadmins":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "disableavatar":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "echo":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "filter":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "general":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "greetmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "history":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "idle":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "ipsend":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "kewltext":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "kick":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "kiddy":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "lastseen":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "link":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "listbans":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "listgreetmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "listquarantined":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "listrangebans":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "listurls":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "loadmotd":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "loadtemplate":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "logsend":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "lower":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "move":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "mtimeout":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "muzzle":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "oldname":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "paint":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "pmgreetmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "pmroom":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "rangeban":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "rangeunban":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "redirect":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "remgreetmsg":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "remtopic":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "remurl":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "roominfo":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "roomsearch":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "sharefiles":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "stats":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "status":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "stealth":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "trace":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "unban":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "uncustomname":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "unlink":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "unlower":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "unquarantine":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "urban":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "url":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "vspy":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "whois":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}
				case "whowas":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val, "commands");
					break;
				}

					#endregion

					#region Advanced

				case "EnableFileBrowse":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set(model.Name, val);
					break;
				}
				case "MaskIPs":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set(model.Name, val);
					break;
				}
				case "EnableRoomSearch":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("roomsearch", val);
					break;
				}
				case "LocalLogin":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set(model.Name, val);
					break;
				}
				case "ib0tUrl":
				{
					Settings.Set("url", model.Value, "web");
					break;
				}
				case "UDPEndPoint":
				{
					IPAddress ip;

					if (IPAddress.TryParse((string) model.Value, out ip))
					{
						Settings.Set(model.Name, ip.GetAddressBytes());
					}

					break;
				}
				case "Language":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set(model.Name, val);
					break;
				}

					#endregion

					#region Scripting

				case "JSEnable":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("scripting", val);
					break;
				}
				case "InRoomScripting":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("inroom_scripting", val);
					break;
				}
				case "ScriptLevelChange":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("script_can_level", val);
					break;
				}
				case "Scripting Level":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set("inroom_level", val);
					break;
				}

					#endregion

					#region Captcha

				case "CaptchaEnabled":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("captcha", val);
					break;
				}
				case "CaptchaMode":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set("captcha_mode", val);
					break;
				}

					#endregion

					#region Restrictions

				case "AgeRestrictEnabled":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("age_restrict", val);
					break;
				}
				case "RejectMale":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("reject_male", val);
					break;
				}
				case "RejectFemale":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("reject_female", val);
					break;
				}
				case "RejectUnknown":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("reject_unknown", val);
					break;
				}
				case "MinAge":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set("age_restrict_value", val);
					break;
				}

					#endregion

					#region Ban Lists

				case "BanClearInterval":
				{
					var val = Convert.ToByte(model.Value);
					Settings.Set("auto_ban_clear_interval", val);
					break;
				}
				case "BanClearEnabled":
				{
					var val = Convert.ToBoolean(model.Value);
					Settings.Set("auto_ban_clear_enabled", val);
					break;
				}

					#endregion
			}
		}
	}
}