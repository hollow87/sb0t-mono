﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sb0tUi.Controllers.api
{
	[Authorize]
    public class ChatLogController : ApiController
    {
		public DateTime[] Get()
		{
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t", "ChatLogs");
			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);

			var files = Directory.EnumerateFiles(dataDir, "ChatLog-*.log");

			List<DateTime> dates = new List<DateTime>();
			foreach (var file in files)
			{
				var fileNoExt = file.Replace(".log", "");

				var fileParts = fileNoExt.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
				if (fileParts.Length < 4)
					return null;

				int year = int.Parse(fileParts[1]);
				int month = int.Parse(fileParts[2]);
				int day = int.Parse(fileParts[3]);

				DateTime date = new DateTime(year, month, day);
				dates.Add(date);
			}

			return dates.ToArray();
		}

		public string[] Get(DateTime date)
		{
			string dataDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sb0t", "ChatLogs");
			if (!Directory.Exists(dataDir))
				Directory.CreateDirectory(dataDir);


			string fileName = string.Format("ChatLog-{0:yyyy-MM-dd}.log", date);

			string filePath = Path.Combine(dataDir, fileName);
			if (File.Exists(filePath))
				return File.ReadAllLines(filePath);

			return null;
		}
    }
}
