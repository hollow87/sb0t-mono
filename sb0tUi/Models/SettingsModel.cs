﻿namespace sb0tUi.Models
{
	public class SettingsModel
	{
		public string Name { get; set; }

		public object Label { get; set; }

		public object Value { get; set; }

		public string Section { get; set; }

	}
}