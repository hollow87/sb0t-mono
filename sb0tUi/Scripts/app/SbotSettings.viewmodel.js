﻿/// <reference path="../jquery-1.8.2.js" />
/// <reference path="../knockout-2.2.0.js" />
/// <reference path="../jquery.ajax-retry.min.js" />


var DropDownSelectObject = function(id, name) {
    this.id = id;
    this.name = name;
};

function SbotSetting(setting) {
    var self = this;

    self.name = ko.observable(setting.Name);

    if (typeof (setting.Label) == "object") {
        self.label = [];
        for (var i in setting.Label) {
            self.label.push(new DropDownSelectObject(i, setting.Label[i]));
            for (var j = 0; j < self.label.length; j++) {
                if(setting.Value == self.label[j].id)
                    self.value = ko.observable(self.label[j]);
            }
            
        }
    } else {
        self.label = ko.observable(setting.Label);
        self.value = ko.observable(setting.Value);
    }
    
    self.section = ko.observable(setting.Section);
}

function SbotUiViewModel() {
    var self = this;

    // Settings
    self.settings = ko.observableArray([]);
    self.settingsStrings = ko.observableArray([]);
    self.settingsBooleans = ko.observableArray([]);
    self.settingsNumbers = ko.observableArray([]);
    self.settingsObjects = ko.observableArray([]);

    // ServerLog
    self.serverLogDates = [];
    self.serverLogFile = ko.observableArray([]);

    // Admin Log
    self.adminLogDates = [];
    self.adminLogFile = ko.observableArray([]);

    // Chat Log
    self.chatLogDates = [];
    self.chatLogFile = ko.observableArray([]);

    // Motd
    self.motdLines = ko.observable("");

    // string Template
    self.templateLines = ko.observableArray([]);

    // Admin page pagination
    self.pageNumber = ko.observable(0);
    self.numPerPage = 10;
    self.totalPages = ko.computed(function() {
        var div = Math.floor(self.settingsObjects().length / self.numPerPage);
        div += self.settingsObjects().length % self.numPerPage > 0 ? 1 : 0;
        return div;
    });
    self.paginated = ko.computed(function() {
        var first = self.pageNumber() * self.numPerPage;
        return self.settingsObjects.slice(first, first + self.numPerPage);
    });
    self.hasPrevious = ko.computed(function () {
        return self.pageNumber() !== 0;
    });
    self.hasNext = ko.computed(function () {
        return self.pageNumber() !== self.totalPages() - 1;
    });
    self.next = function () {
        if (self.pageNumber() < self.totalPages()) {
            self.pageNumber(self.pageNumber() + 1);
        }
    }
    this.previous = function () {
        if (self.pageNumber() != 0) {
            self.pageNumber(self.pageNumber() - 1);
        }
    }

    // UI Settings
    self.navSections = ["Settings", "Server Log", "Admin Log", "Chat Log", "Motd", "Strings Template"];
    self.navSubSections = ["Main", "Admin", "Advanced", "Scripting", "Captcha", "Restrictions", "Ban List", "Avatar"];

    self.navSectionCurrent = ko.observable();
    self.navSubSectionCurrent = ko.observable();

    self.isServerRunning = ko.observable(false);

    // Behaviours
    self.changeNavSection = function (section) {
        self.navSectionCurrent(section);
    };

    self.changeNavSubSection = function(section) {
        self.navSubSectionCurrent(section);
        if (section == self.navSubSections[7]) {
            init();
            return;
        }
            

        self.loadSettings(self.navSubSections.indexOf(section), "string");
        self.loadSettings(self.navSubSections.indexOf(section), "boolean");
        self.loadSettings(self.navSubSections.indexOf(section), "object");
    };

    self.loadSettings = function(section, type) {
        var url = '/api/SbotSettings?section=' + section + '&type=' + type;
        $.ajax({
            url: url,
            dataType: "json",
            success: function(settings) {
                if (type == "string") {
                    self.settingsStrings.removeAll();
                    $.each(settings, function(index, item) {
                        var obj = new SbotSetting(item);

                        obj.value.subscribe(function(newValue) {
                            $.post('/api/SbotSettings', this, null);
                        }, obj);

                        self.settingsStrings.push(obj);
                    });
                }
                if (type == "boolean") {
                    self.settingsBooleans.removeAll();
                    $.each(settings, function(index, item) {
                        var obj = new SbotSetting(item);

                        obj.value.subscribe(function(newValue) {
                            $.post('/api/SbotSettings', this, null);
                        }, obj);

                        self.settingsBooleans.push(obj);
                    });
                }
                if (type == "object") {
                    self.settingsObjects.removeAll();
                    if (settings != null) {
                        $.each(settings, function(index, item) {
                            var obj = new SbotSetting(item);

                            obj.value.subscribe(function(newValue) {


                                if (typeof (newValue) == "object") {
                                    var data =
                                    {
                                        name: obj.name(),
                                        label: obj.label,
                                        section: obj.section(),
                                        value: obj.value().id
                                    };

                                    $.post('/api/SbotSettings', data, null);
                                }
                            }, obj);

                            self.settingsObjects.push(obj);
                        });
                    }
                }
            }
        }).retry({ times: 3 });
    };


    self.changeNavSection(self.navSections[0]);
    self.changeNavSubSection(self.navSubSections[0]);
};